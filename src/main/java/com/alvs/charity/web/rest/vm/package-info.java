/**
 * View Models used by Spring MVC REST controllers.
 */
package com.alvs.charity.web.rest.vm;
