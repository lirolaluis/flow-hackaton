INSERT INTO public.authority(name)
VALUES ('ROLE_USER'),
       ('ROLE_ADMIN');

INSERT INTO public.user(login, password_hash, first_name, last_name, email, created_by, created_date, activated,
                     lang_key)
VALUES ('admin', '$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC', 'Administrator', 'Administrator',
        'admin@localhost', 'system', CURRENT_TIMESTAMP, '1', 'en');

INSERT INTO public.user_authority(user_id, authority_id)
VALUES (1, 1),
       (1, 2);
