-- CREATE TABLE "user" -----------------------------
CREATE TABLE public.user
(
    user_id            SERIAL UNIQUE          NOT NULL,
    login              CHARACTER VARYING(100) NOT NULL,
    password_hash      CHARACTER VARYING(60),
    first_name         CHARACTER VARYING(50),
    last_name          CHARACTER VARYING(50),
    email              CHARACTER VARYING(100),
    image_url          CHARACTER VARYING(256),
    activated          BOOLEAN                NOT NULL,
    lang_key           CHARACTER VARYING(6),
    activation_key     CHARACTER VARYING(20),
    reset_key          CHARACTER VARYING(20),
    created_by         CHARACTER VARYING(50)  NOT NULL,
    created_date       TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    reset_date         TIMESTAMP WITHOUT TIME ZONE,
    last_modified_by   CHARACTER VARYING(50),
    last_modified_date TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT pk_user PRIMARY KEY (user_id),
    CONSTRAINT ux_user_email UNIQUE (email),
    CONSTRAINT ux_user_login UNIQUE (login)
);


-- CREATE TABLE "authority" ------------------------------------
CREATE TABLE public.authority
(
    authority_id SERIAL UNIQUE NOT NULL,
    name         VARCHAR(50)   NOT NULL
);

-- CREATE TABLE "user_authority" -------------------------------
CREATE TABLE public.user_authority
(
    user_id      BIGINT NOT NULL,
    authority_id BIGINT NOT NULL,
    CONSTRAINT fk_user_id FOREIGN KEY (user_id)
        REFERENCES public.user (user_id),
    CONSTRAINT authority_id FOREIGN KEY (authority_id)
        REFERENCES public.authority (authority_id)
);
